/**
 * đầu vào là số có 2 chữ số mà người dùng nhập 
 * 
 * xử lý : 
 * gán số có 2 chữ số vào biến numberUser 
 * ép kiểu từ string sang number bằng cách * 1 
 * tách chữ số hàng đơn vị bằng cách lấy numberUser % 10 để lấy phần dư rồi gán vào biến hangDonVi
 * tách chữ số hàng chục bằng cách lấy numberUser / 10 rồi Math.floor để lấy phần thương rồi gán vào biến hangChuc 
 * cộng hàng đơn vị và hàng chục 
 * 
 */

var hangDonVi , hangChuc , ketQua5 , numberUser ; 
function tinhTong() {
    numberUser = document.getElementById('haikyso'); 
    numberUser = numberUser.value * 1 ; 
    hangDonVi = numberUser % 10 ; 
    hangChuc = Math.floor(numberUser / 10) ;
    ketQua5 = hangDonVi + hangChuc ; 
    document.getElementById('show-ketqua5').innerHTML = `Tổng 2 số là : ${ketQua5}`; 
}