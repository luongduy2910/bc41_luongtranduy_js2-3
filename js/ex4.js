/**
 * đầu vào là chiều dài và chiều rộng mà người dùng nhập 
 * 
 * xử lý : 
 * gán chiều dài và chiều rộng vào 2 biến lần lượt là chieuDai và chieuRong 
 * ép kiểu từ string sang number bằng cách * 1 
 * sử dụng công thức để tính chu vi và diện tích rồi gán vào 2 biến chuVi và dienTich 
 */

var chieuDai , chieuRong , chuVi , dienTich ; 
function tinhChuDien() {
    chieuDai = document.getElementById('chieudai') ; 
    chieuDai = chieuDai.value * 1 ;
    chieuRong = document.getElementById('chieurong') ; 
    chieuRong = chieuRong.value * 1 ; 
    chuVi = new Intl.NumberFormat('vn-Vn').format((chieuDai + chieuRong)*2) ; 
    dienTich = new Intl.NumberFormat('vn-Vn').format(chieuDai * chieuRong); 
    document.getElementById('show-ketqua4').innerHTML = `Chu vi: ${chuVi} , Dien tich: ${dienTich}`;

}