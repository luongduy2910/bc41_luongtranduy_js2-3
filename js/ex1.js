/**
 * đầu vào là lương một ngày và số ngày làm mà người dùng nhập vào 
 * 
 * xử lý: 
 * Gán 2 giá trị mà người dùng nhập vào 2 biến tienLuong và ngayLam
 * Ép kiểu dữ liệu từ dạng chuỗi sang dạng số bằng cách *1 
 * tính theo công thức rồi gán vào biến ketQua, sử dụng innerHTML để truyền dữ liệu vào ô hiển thị kết quả
 */

var tienLuong , ngayLam , ketQua ; 
function tinhTienLuong() {
    tienLuong = document.getElementById('tien__luong');
    tienLuong = tienLuong.value * 1;
    ngayLam = document.getElementById('ngay__lam');
    ngayLam = ngayLam.value * 1 ; 
    ketQua = new Intl.NumberFormat('vn-VN').format(tienLuong * ngayLam);
    document.getElementById('show-ketqua').innerHTML = `Tiền lương của bạn tháng này là : ${ketQua}`
}