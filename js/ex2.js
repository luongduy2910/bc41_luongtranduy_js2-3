/**
 * đầu vào là 5 số thực mà người dùng nhập 
 * 
 * xử lý : 
 * gán 5 số vào 5 biến khác nhau lần lượt là so1 , so2 , so3 , so4 , so5 
 * ép kiểu dữ liệu từ chuỗi sang số bằng cách * 1 
 * tính giá trị trung bình theo công thức rồi gán vào biến ketQua 
 * truyền dữ liệu bằng innerHTML 
 */

var so1 , so2 , so3 , so4 , so5 , ketQua2; 
function tinhGiaTriTb() {
    so1 = document.getElementById('so1');
    so1 = so1.value * 1 ;
    so2 = document.getElementById('so2');
    so2 = so2.value * 1 ;
    so3 = document.getElementById('so3');
    so3 = so3.value * 1 ;
    so4 = document.getElementById('so4');
    so4 = so4.value * 1 ;
    so5 = document.getElementById('so5');
    so5 = so5.value * 1 ;
    ketQua2 = new Intl.NumberFormat('vn-Vn').format((so1 + so2 + so3 + so4 + so5)/5);
    document.getElementById('show-ketqua2').innerHTML = `Giá trị trung bình là ${ketQua2}`;
}